FROM node:lts-alpine
RUN apk update && apk upgrade

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ARG API_URL

COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
RUN npm run generate

EXPOSE 3000
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

CMD [ "npm", "start" ]