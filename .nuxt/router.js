import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _472654e2 = () => interopDefault(import('../src/pages/about-me/index.vue' /* webpackChunkName: "pages/about-me/index" */))
const _6fbc996c = () => interopDefault(import('../src/pages/contact/index.vue' /* webpackChunkName: "pages/contact/index" */))
const _1ef73f58 = () => interopDefault(import('../src/pages/portfolio/index.vue' /* webpackChunkName: "pages/portfolio/index" */))
const _6656a918 = () => interopDefault(import('../src/pages/services/website/index.vue' /* webpackChunkName: "pages/services/website/index" */))
const _e6d33088 = () => interopDefault(import('../src/pages/portfolio/_id.vue' /* webpackChunkName: "pages/portfolio/_id" */))
const _0f43ccca = () => interopDefault(import('../src/pages/index.vue' /* webpackChunkName: "pages/index" */))
const _25a6751a = () => interopDefault(import('../src/pages/*.vue' /* webpackChunkName: "pages/*" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about-me",
    component: _472654e2,
    name: "about-me"
  }, {
    path: "/contact",
    component: _6fbc996c,
    name: "contact"
  }, {
    path: "/portfolio",
    component: _1ef73f58,
    name: "portfolio"
  }, {
    path: "/services/website",
    component: _6656a918,
    name: "services-website"
  }, {
    path: "/portfolio/:id",
    component: _e6d33088,
    name: "portfolio-id"
  }, {
    path: "/",
    component: _0f43ccca,
    name: "index"
  }, {
    path: "/*",
    component: _25a6751a,
    name: "*"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
